package com.sixletterwords;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/** Class is implementation of AlgorithmStrategy. Has process method to find words from
 * input which have given length and consist of two shorter words. It uses ArrayList for
 * process and present output.
 **/
public class AlgorithmStrategyArrayList implements AlgorithmStrategy {

    private List<String> results = new ArrayList<>();
    private List<String> words = new ArrayList<>();
    private Set<String> semiWords = new HashSet<>();
    private static final int wordLength = 6;

    @Override
    public List<String> process(BufferedReader bufferedReader) throws IOException {

        String currentLine;

        while ((currentLine = bufferedReader.readLine()) != null) {
            if (currentLine.length() == wordLength) {
                words.add(currentLine);
            } else if (currentLine.length() < wordLength) {
                semiWords.add(currentLine);
            }
        }

        for (String word : words) {
            for (int i = 1; i < wordLength; i++) {
                String firstPart = new StringBuilder(word).substring(0, i);
                String secondPart = new StringBuilder(word).substring(i);
                if (semiWords.contains(firstPart) && semiWords.contains(secondPart)) {
                    results.add(firstPart + " + " + secondPart + " -> " + word);
                }
            }
        }
        return results;
    }
}
