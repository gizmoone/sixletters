package com.sixletterwords;

import java.io.IOException;
import java.net.URL;

/**
 * Class with main method.
 */
public class Application {

    public static void main(String[] args) throws IOException {

        long startTime1 = System.currentTimeMillis();

        String filePath = "/wordlist.txt";
        DataLoaderFromFile dataLoaderFromFile = new DataLoaderFromFile(filePath);
        AlgorithmStrategyArrayList dataProcessArrayList = new AlgorithmStrategyArrayList();
        DataPrinterOnScreen dataPrinterOnScreen = new DataPrinterOnScreen();
        AlgorithmExecutor algorithmExecutor = new AlgorithmExecutor(dataLoaderFromFile, dataProcessArrayList, dataPrinterOnScreen);
        algorithmExecutor.execute();

        long endTime1 = System.currentTimeMillis();


        long startTime2 = System.currentTimeMillis();

        AlgorithmStrategyLinkedList dataProcessLinkedList = new AlgorithmStrategyLinkedList();
        algorithmExecutor = new AlgorithmExecutor(dataLoaderFromFile, dataProcessLinkedList, dataPrinterOnScreen);
        algorithmExecutor.execute();

        long endTime2 = System.currentTimeMillis();


        long startTime3 = System.currentTimeMillis();

        URL url = new URL("http://codekata.com/data/wordlist.txt");
        DataPrinterToFile dataPrinterToFile = new DataPrinterToFile();
        DataLoaderFromUrl dataLoaderFromUrl = new DataLoaderFromUrl(url);
        algorithmExecutor = new AlgorithmExecutor(dataLoaderFromUrl, dataProcessLinkedList, dataPrinterToFile);
        algorithmExecutor.execute();

        long endTime3 = System.currentTimeMillis();

        System.out.println("First execution time: " + (endTime1-startTime1) + "ms");
        System.out.println("Second execution time: " + (endTime2-startTime2) + "ms");
        System.out.println("Third execution time: " + (endTime3-startTime3) + "ms");
    }
}
