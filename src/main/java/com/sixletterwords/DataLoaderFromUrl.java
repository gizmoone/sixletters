package com.sixletterwords;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Class includes method for load input data from given url.
 */
public class DataLoaderFromUrl implements DataLoader{

    private URL url;

    public DataLoaderFromUrl(URL url) throws MalformedURLException {
        this.url = url;
    }

    @Override
    public BufferedReader load() throws IOException {
        BufferedReader br;
        br = new BufferedReader(new InputStreamReader(url.openStream(), "Cp1252"));
        return br;
    }
}
