package com.sixletterwords;

import java.io.*;

/**
 * Class includes method for load input data from given filepath.
 */
public class DataLoaderFromFile implements DataLoader {

    private String filePath;

    public DataLoaderFromFile(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public BufferedReader load() throws UnsupportedEncodingException {
        BufferedReader br;
        br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(filePath), "Cp1252"));
        return br;
    }
}