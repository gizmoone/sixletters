package com.sixletterwords;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Class includes method which save output as text file.
 */
public class DataPrinterToFile implements DataPrinter {

    @Override
    public void print(List<String> results) throws IOException {

        Path output = Paths.get("outputWordList.txt");
        Files.write(output, results, Charset.forName("Cp1252"));
    }
}
