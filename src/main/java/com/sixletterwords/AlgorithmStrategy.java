package com.sixletterwords;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

/** Interface which defines method for processing algorithm.
 */
public interface AlgorithmStrategy {

    List<String> process(BufferedReader bufferedReader) throws IOException;
}
