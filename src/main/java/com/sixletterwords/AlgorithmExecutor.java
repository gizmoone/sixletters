package com.sixletterwords;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

/** Class which has constructor to create application from three components: input (DataLoader),
 * algorithm (AlgorithmStrategy) and output(DataPrinter). Has method to execute application.
 **/
public class AlgorithmExecutor {

    private DataLoader dataLoader;
    private AlgorithmStrategy algorithmStrategy;
    private DataPrinter dataPrinter;

    public AlgorithmExecutor(DataLoader dataLoader, AlgorithmStrategy algorithmStrategy, DataPrinter dataPrinter) {
        this.dataLoader = dataLoader;
        this.algorithmStrategy = algorithmStrategy;
        this.dataPrinter = dataPrinter;
    }

    public void execute() throws IOException {
        BufferedReader reader = dataLoader.load();
        List<String> results = algorithmStrategy.process(reader);
        dataPrinter.print(results);
    }
}
