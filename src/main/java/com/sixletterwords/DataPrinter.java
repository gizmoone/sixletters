package com.sixletterwords;

import java.io.IOException;
import java.util.List;

/**
 * Interface defines method to print output data.
 */
public interface DataPrinter {

    void print(List<String> results) throws IOException;
}
