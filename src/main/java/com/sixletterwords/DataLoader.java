package com.sixletterwords;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Interface that defines methods for loading input data.
 */
public interface DataLoader {

    BufferedReader load() throws IOException;
}
