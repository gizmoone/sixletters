package com.sixletterwords;

import java.util.List;

/**
 * Class includes method which prints output data as text on command line interface.
 */
public class DataPrinterOnScreen implements DataPrinter {

    @Override
    public void print(List<String> results) {
        for (String result: results) {
            System.out.println(result);
        }
    }
}
