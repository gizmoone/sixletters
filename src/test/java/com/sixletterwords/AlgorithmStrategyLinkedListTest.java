package com.sixletterwords;

import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.util.LinkedList;
import java.util.List;

public class AlgorithmStrategyLinkedListTest extends BaseTest {

    //From given input file shout match and find three words
    @Test
    public void testIfFindThreeWords() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindThreeWords.txt");

        AlgorithmStrategyLinkedList process = new AlgorithmStrategyLinkedList();

        List<String> result = process.process(bufferedReader);
        Assert.assertTrue("couldn't find correct words", result.size() == 3);
    }

    //From given input file shouldn't match any word
    @Test
    public void testIfFindNoWords() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindNoWords.txt");

        AlgorithmStrategyLinkedList process = new AlgorithmStrategyLinkedList();

        List<String> result = process.process(bufferedReader);
        Assert.assertTrue("found wrong words", result.isEmpty());
    }

    //From given input file (where are words which match to seven char words) shouldn't find any result
    @Test
    public void testIfDoesntFindSevenCharWords() throws Exception{
        BufferedReader bufferedReader = load("/TestIfDoesntFindSevenCharWords.txt");

        AlgorithmStrategyLinkedList process = new AlgorithmStrategyLinkedList();

        List<String> result = process.process(bufferedReader);
        Assert.assertTrue("found seven chars words", result.isEmpty());
    }

    //From given input should find word consist of one and five characters
    @Test
    public void testIfFindWordOnePlusFiveChars() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindWordOnePlusFiveChars.txt");

        AlgorithmStrategyLinkedList process = new AlgorithmStrategyLinkedList();

        List<String> result = process.process(bufferedReader);

        Assert.assertEquals("[d + oolee -> doolee]", result.toString());
    }

    //From given input should find word consist of two and four characters
    @Test
    public void testIfFindWordTwoPlusFourChars() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindWordTwoPlusFourChars.txt");

        AlgorithmStrategyLinkedList process = new AlgorithmStrategyLinkedList();

        List<String> result = process.process(bufferedReader);

        Assert.assertEquals("[do + olee -> doolee]", result.toString());
    }

    //From given input should find word consist of three and three characters
    @Test
    public void testIfFindWordThreePlusThreeChars() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindWordThreePlusThreeChars.txt");

        AlgorithmStrategyLinkedList process = new AlgorithmStrategyLinkedList();

        List<String> result = process.process(bufferedReader);

        Assert.assertEquals("[doo + lee -> doolee]", result.toString());
    }

    //From given input should find word consist of four and two characters
    @Test
    public void testIfFindWordFourPlusTwoChars() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindWordFourPlusTwoChars.txt");

        AlgorithmStrategyLinkedList process = new AlgorithmStrategyLinkedList();

        List<String> result = process.process(bufferedReader);

        Assert.assertEquals("[dool + ee -> doolee]", result.toString());
    }

    //From given input should find word consist of five and one characters
    @Test
    public void testIfFindWordFivePlusOneChars() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindWordFivePlusOneChars.txt");

        AlgorithmStrategyLinkedList process = new AlgorithmStrategyLinkedList();

        List<String> result = process.process(bufferedReader);

        Assert.assertEquals("[doole + e -> doolee]", result.toString());
    }

    //From given input should match three the same words consists of different semi words combination
    @Test
    public void TestIfFindThreeSameWords() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindThreeSameWords.txt");

        List<String> expect = new LinkedList<>();
        expect.add("d + oolee -> doolee");
        expect.add("do + olee -> doolee");
        expect.add("doo + lee -> doolee");

        AlgorithmStrategyLinkedList process = new AlgorithmStrategyLinkedList();

        List<String> result = process.process(bufferedReader);

        Assert.assertEquals(expect, result);
    }
}
