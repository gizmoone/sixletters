package com.sixletterwords;

import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;

public class DataLoaderFromFileTest {

    //Test for checking if method can load given file
    @Test
    public void testIfCanLoadFile() throws Exception{

        DataLoaderFromFile load = new DataLoaderFromFile("/TestIfCanLoadFile.txt");
        BufferedReader bufferedReader = load.load();
        String expect = "This is file context";
        String result = bufferedReader.readLine();

        Assert.assertEquals(expect, result);
    }
}
