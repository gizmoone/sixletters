package com.sixletterwords;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Base class for testing algorithm in isolation. It includes method which load input data.
 */
public class BaseTest {

    public BufferedReader load(String filePath) throws UnsupportedEncodingException {

        BufferedReader br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(filePath), "Cp1252"));
        return br;
    }
}
