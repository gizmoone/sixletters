package com.sixletterwords;

import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

public class AlgorithmStrategyArrayListTest extends BaseTest {

    //This test should find and match three words
    @Test
    public void testIfFindThreeWords() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindThreeWords.txt");

        AlgorithmStrategyArrayList process = new AlgorithmStrategyArrayList();

        List<String> result = process.process(bufferedReader);
        Assert.assertTrue("couldn't find correct words", result.size() == 3);
    }

    //This test shouldn't match any word
    @Test
    public void testIfFindNoWords() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindNoWords.txt");

        AlgorithmStrategyArrayList process = new AlgorithmStrategyArrayList();

        List<String> result = process.process(bufferedReader);
        Assert.assertTrue("found wrong words", result.isEmpty());
    }

    //This test shouldn't find any words(input includes only seven char words and semi words to match them)
    @Test
    public void testIfDoesntFindSevenCharWords() throws Exception{
        BufferedReader bufferedReader = load("/TestIfDoesntFindSevenCharWords.txt");

        AlgorithmStrategyArrayList process = new AlgorithmStrategyArrayList();

        List<String> result = process.process(bufferedReader);
        Assert.assertTrue("found seven chars words", result.isEmpty());
    }

    //Test should find word consist of one and five characters
    @Test
    public void testIfFindWordOnePlusFiveChars() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindWordOnePlusFiveChars.txt");

        AlgorithmStrategyArrayList process = new AlgorithmStrategyArrayList();

        List<String> result = process.process(bufferedReader);

        Assert.assertEquals("[d + oolee -> doolee]", result.toString());
    }

    //Test should find word consist of two and four characters
    @Test
    public void testIfFindWordTwoPlusFourChars() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindWordTwoPlusFourChars.txt");

        AlgorithmStrategyArrayList process = new AlgorithmStrategyArrayList();

        List<String> result = process.process(bufferedReader);

        Assert.assertEquals("[do + olee -> doolee]", result.toString());
    }

    //From given input should find word consist of three and three characters
    @Test
    public void testIfFindWordThreePlusThreeChars() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindWordThreePlusThreeChars.txt");

        AlgorithmStrategyArrayList process = new AlgorithmStrategyArrayList();

        List<String> result = process.process(bufferedReader);

        Assert.assertEquals("[doo + lee -> doolee]", result.toString());
    }

    //From given input should find word consist of four and two characters
    @Test
    public void testIfFindWordFourPlusTwoChars() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindWordFourPlusTwoChars.txt");

        AlgorithmStrategyArrayList process = new AlgorithmStrategyArrayList();

        List<String> result = process.process(bufferedReader);

        Assert.assertEquals("[dool + ee -> doolee]", result.toString());
    }

    //From given input should find word consist of five and one characters
    @Test
    public void testIfFindWordFivePlusOneChars() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindWordFivePlusOneChars.txt");

        AlgorithmStrategyArrayList process = new AlgorithmStrategyArrayList();

        List<String> result = process.process(bufferedReader);

        Assert.assertEquals("[doole + e -> doolee]", result.toString());
    }

    //Test should match three the same words consists of different semi words combination
    @Test
    public void TestIfFindThreeSameWords() throws Exception{
        BufferedReader bufferedReader = load("/TestIfFindThreeSameWords.txt");

        List<String> expect = new ArrayList<>();
        expect.add("d + oolee -> doolee");
        expect.add("do + olee -> doolee");
        expect.add("doo + lee -> doolee");

        AlgorithmStrategyArrayList process = new AlgorithmStrategyArrayList();

        List<String> result = process.process(bufferedReader);

        Assert.assertEquals(expect, result);
    }
}
