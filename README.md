# Six Letter Words #


## Quick summary ##

This repository includes application which is an answer for problem described on http://codekata.com/kata/kata08-conflicting-objectives/.

Write program that process dictionary from given file and find all six letter words which are composed of two concatenated smaller words.
Write the program three times.

1. The first time, make program as readable as you can make it.
2. The second time, optimize the program to run as fast as you can make it.
3. The third time, write as extendable program as you can.

### Short description of my solutions ###

1. I made program divided on three parts. Loading input , process algorithm and printing output part. Then created constructor with all those objects and execute methods on them.
2. To make program faster I used LinkedLists instead of ArrayLists.
3. Because of application construction I could extend program by new solutions such as load input data from URL and print output to the text file.

Program execute all those three solutions in raw. At the end it prints execution times for each of those.

## Set up tips ##

Application is build on Maven. You can run it as typical Maven application.
Tests are created in Junit. You can use Maven to run the tests.
